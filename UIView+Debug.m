//
//  UIView+Debug.m
//  Objective-C Utilities
//
//  Created by Henrik Söderqvist on 2013-06-27.
//  Copyright (c) 2013 Henrik Söderqvist. All rights reserved.
//

#import "UIView+Debug.h"

@implementation UIView (Debug)

- (void)findMisbehavingScrollViews
{
    UIView *view = [[UIApplication sharedApplication] keyWindow];
    [self findMisbehavingScrollViewsIn:view];
}

- (void)findMisbehavingScrollViewsIn:(UIView *)view
{
    if ([view isKindOfClass:[UIScrollView class]])
    {
        NSLog(@"Found UIScrollView: %@", view);
        if ([(UIScrollView *)view scrollsToTop])
        {
            NSLog(@"scrollsToTop = YES!");
        }
    }
    for (UIView *subview in [view subviews])
    {
        [self findMisbehavingScrollViewsIn:subview];
    }
}

// More geeneric code is work in progress
//- (void)findMisbehavingScrollViewsNotScrollingToTop
//{
//    [self findMisbehavingViewsOfClass:[UIScrollView class] forSelector:@selector(scrollsToTop) notMatchingBOOLValue:NO];
//}
//
//- (void)findMisbehavingViewsOfClass:(Class)aClass forSelector:(SEL)aSelector notMatchingBOOLValue:(BOOL)aBoolValue
//{
//    UIView *view = [[UIApplication sharedApplication] keyWindow];
//    [self findMisbehavingViewsOfClass:aClass forSelector:aSelector notMatchingBOOLValue:aBoolValue inView:view];
//}
//
//- (void)findMisbehavingViewsOfClass:(Class)aClass forSelector:(SEL)aSelector notMatchingBOOLValue:(BOOL)boolValue inView:(UIView *)view
//{
//    if ([view isKindOfClass:aClass])
//    {
//        if ([view respondsToSelector:aSelector])
//        {
//            NSMethodSignature *signature = [view methodSignatureForSelector:aSelector];
//            NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
//            BOOL b = boolValue;
//            
//            [invocation setTarget:view];
//            [invocation setSelector:aSelector];
//            [invocation invoke];
//            [invocation getReturnValue:&b];
//            
//            if (b != boolValue)
//            {
//                NSLog(@"Found %@: %@ (%@ = %@)", NSStringFromClass([aClass class]), view, NSStringFromSelector(aSelector), (boolValue ? @"YES" : @"NO"));
//            }
//            else
//            {
//                NSLog(@"Found %@: %@", aClass, view);
//            }
//        }
//    }
//    
//    for (UIView *subview in [view subviews])
//    {
//        [self findMisbehavingViewsOfClass:aClass forSelector:aSelector notMatchingBOOLValue:boolValue inView:view];
//    }
//}

@end
