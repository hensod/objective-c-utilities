/*
 *  Debug.h
 *  Objective-C Utilities
 *
 *  Created by Henrik Söderqvist on 10/26/09.
 *  Copyright 2013 Henrik Söderqvist. All rights reserved.
 *
 */

// Macros used for automatically disabling debug prints in release builds
// Add -DDEBUGBUILD to C flags in project settings for desired build variants.
#ifdef DEBUGBUILD
#define DEBUGLOG(format, ...) NSLog(@"%@(%d):" format, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, ## __VA_ARGS__)
#else
#define DEBUGLOG(format, ...)
#endif
