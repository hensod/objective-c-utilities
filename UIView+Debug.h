//
//  UIView+Debug.h
//  Objective-C Utilities
//
//  Created by Henrik Söderqvist on 2013-06-27.
//  Copyright (c) 2013 Henrik Söderqvist. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Debug)

- (void)findMisbehavingScrollViews;
- (void)findMisbehavingScrollViewsIn:(UIView *)view;

- (void)findMisbehavingScrollViewsNotScrollingToTop;

- (void)findMisbehavingViewsOfClass:(Class)aClass forSelector:(SEL)aSelector notMatchingBOOLValue:(BOOL)aBoolValue;
- (void)findMisbehavingViewsOfClass:(Class)aClass forSelector:(SEL)aSelector notMatchingBOOLValue:(BOOL)boolValue inView:(UIView *)view;

@end
